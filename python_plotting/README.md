- python_plotting: it contains a jupyter notebook that generates all the plots in the paper.

 To run the Python code, create a virtual environment with   
 Anaconda, activate it and install the packages contained in the requirements file:
 
    conda create -n GoOSE_adaptive_control python=3.8
    conda activate GoOSE_adaptive_control
    python -m pip install -r requirements.txt