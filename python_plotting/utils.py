from matplotlib import rcParams

__all__ = ['set_figure_params', 'hide_spines', 'hide_all_ticks']

def set_figure_params(serif=True, fontsize=9):
    """Define default values for font, fontsize and use latex
    Parameters
    ----------
    serif: bool, optional
        Whether to use a serif or sans-serif font
    """

    params = {
              'font.serif': ['Times',
                             'Palatino',
                             'New Century Schoolbook',
                             'Bookman',
                             'Computer Modern Roman'] +  rcParams['font.serif'],
              'font.sans-serif': ['Times',
                                  'Helvetica',
                                  'Avant Garde',
                                  'Computer Modern Sans serif'] + rcParams['font.sans-serif'],
              'font.family': 'serif',
              'text.usetex': True,
              # Make sure mathcal doesn't use the Times style
              'text.latex.preamble':
              r'\DeclareMathAlphabet{\mathcal}{OMS}{cmsy}{m}{n}',

              'axes.labelsize': fontsize,
              'axes.linewidth': .75,

              'font.size': fontsize,
              'legend.fontsize': fontsize,
              'xtick.labelsize': fontsize * 8 / 9,
              'ytick.labelsize': fontsize * 8 / 9,

              # 'figure.dpi': 150,
              # 'savefig.dpi': 600,
              'legend.numpoints': 1,
              }

    if not serif:
        params['font.family'] = 'sans-serif'

    rcParams.update(params)


def hide_all_ticks(axis):
    """Hide all ticks on the axis.
    Parameters
    ----------
    axis: matplotlib axis
    """
    axis.tick_params(axis='both',        # changes apply to the x-axis
                     which='both',       # affect both major and minor ticks
                     bottom=False,       # ticks along the bottom edge are off
                     top=False,          # ticks along the top edge are off
                     left=False,         # No ticks left
                     right=False,        # No ticks right
                     labelbottom=False,  # No tick-label at bottom
                     labelleft=False)    # No tick-label at bottom


def hide_spines(axis, top=True, right=True):
    """Hide the top and right spine of the axis."""
    if top:
        axis.spines['top'].set_visible(False)
        axis.xaxis.set_ticks_position('bottom')
    if right:
        axis.spines['right'].set_visible(False)
        axis.yaxis.set_ticks_position('left')
